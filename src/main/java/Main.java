import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] numbers1 = {1, 1, 1}; //111 => 112 = {1,1,2}
        int[] numbers2 = {2, 1, 9}; //219 => 220 = {2,2,0}
        int[] numbers25 = {2, 9, 9};
        int[] numbers26 = {9, 9, 9, 9, 9, 9, 9}; // {3,0,0,0,0,0,0}
        int[] numbers3 = {9, 9, 9}; //999 => 1000 = {1,0,0,0}
        int[] numbersAfterOperation = incrementArray(numbers2);
        System.out.println(Arrays.toString(numbersAfterOperation));
    }

    //{3,0,0,0,0,0,0};
    private static int[] incrementArray(int[] numbers) {
        int[] resultArray = new int[numbers.length];
        System.arraycopy(numbers, 0, resultArray, 0, numbers.length);
        for (int i = resultArray.length - 1; i > -1; i--) {
            if (resultArray[i] != 9) {
                resultArray[i]++;
                break;
            } else {
                resultArray[i] = 0;
            }
        }
        if(resultArray[0] == 0) {
            resultArray = new int[resultArray.length + 1];
            resultArray[0] = 1;
        }
        return resultArray;
    }
}
